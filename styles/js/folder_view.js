'use strict';

var events = require('events');
var fs = require('fs');
var path = require('path');
var jade = require('jade');
var util = require('util');

// Template engine
var gen_files_view = jade.compile([
    '- each file in files',
    '  li(class="pure-menu-item", data-path="#{file.path}")',
    '    a(href="#", class="pure-menu-link") #{file.name}'
].join('\n'));

function Folder(jquery_element) {
  events.EventEmitter.call(this);
  this.element = jquery_element;
  this.folder_path = "";

  var self = this;
  // Double click on blank place
  this.element.parent().parent().parent().on('dblclick', function(e) {
    self.emit('open_folder');
    e.stopPropagation();
  });
  // Click on folder
  this.element.on('click', '.pure-menu-item', function(e) {
    self.element.find('.focus').removeClass('focus');
    $(this).find(".pure-menu-link").addClass('focus');
    let data_path = $(this).attr("data-path");
    self.folder_path = data_path;
    self.emit('list', data_path);
    e.stopPropagation();
  });
  // Double click on folder
  this.element.on('dblclick', '.pure-menu-item', function(e) {
    var folder_path = $(this).attr('data-path');
    self.emit('open_folder', folder_path);
    e.stopPropagation();
  });
}

util.inherits(Folder, events.EventEmitter);

Folder.prototype.open = function(dir, callback) {
  var self = this;
  let default_folder;
  fs.readdir(dir, function(error, files) {
    if (error) {
      console.log(error);
      window.alert(error);
      return;
    }

    // get all folder in the dir
    let record = [];
    for (var i = 0; i < files.length; ++i) {
      let cur_path = path.join(dir, files[i]);
      if (fs.statSync(cur_path).isDirectory() && path.basename(cur_path).split('.')[0] !== '') {
        default_folder = default_folder || cur_path;
        record.push(stat(cur_path));
      }
    }
    self.element.html(gen_files_view({ files: record }));

    if (callback) {
      callback(default_folder);
    }
  });
}

function stat(file_path) {
  return {
    name: path.basename(file_path),
    path: file_path,
  }
}

module.exports = Folder;
