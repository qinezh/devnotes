'use strict';

let events = require('events');
let fs = require('graceful-fs');
let path = require('path');
let jade = require('jade');
let util = require('util');

// Template engine
let gen_files_view = jade.compile(
[
  '- each file in files',
  '  a(href="#")',
  '    div(class="email-item pure-g", data-path="#{file.path}")',
  '      .pure-u-3-4',
  '        h4(class="email-subject") #{file.title}',
  '        p(class="email-desc") #{file.brief}',
].join('\n'));

function Item(jquery_element) {
  events.EventEmitter.call(this);
  this.element = jquery_element;

  let self = this;

  this.element.on('mouseenter', '.email-item', function() {
    $(this).addClass("email-item-selected");
  });

  this.element.on('mouseleave', '.email-item', function() {
    $(this).removeClass("email-item-selected");
  });

  // Click on file
  this.element.on('click', '.email-item', function(e) {
    var file_path = $(this).attr('data-path');
    self.emit('detail', file_path);
  });

  // Double click on file
  this.element.on('dblclick', '.email-item', function(e) {
    var file_path = $(this).attr('data-path');
    self.emit('edit', file_path);
    e.stopPropagation();
  });

  // Double click on extra space
  this.element.on('dblclick', function() {
    let file_path = $(this).children().attr("data-path");
    if (file_path) {
      self.emit('open_folder', file_path);
      e.stopPropagation();
    }
  });
}

util.inherits(Item, events.EventEmitter);

Item.prototype.open = function(dir, callback) {
  let self = this;
  let default_file;
  fs.readdir(dir, function(err, files) {
    if (err) {
      throw new Error(err);
    }

    for (let i = 0; i < files.length; ++i) {
      let cur_path = path.join(dir, files[i]);
      if (fs.statSync(cur_path).isFile()) {
        files[i] = stat(cur_path);
        default_file = default_file || cur_path;
      }
    }

    self.element.html(gen_files_view({files: files}));
    if (callback) {
      callback(default_file);
    }
  });
}

function stat(file_path) {
  let data = extract(file_path);
  return {
    path: file_path,
    title: data.title,
    brief: data.brief,
  }
}

function extract(file_path) {
  if (!fs.lstatSync(file_path).isFile()) {
    throw(new Error("Not a file"));
  }
  let data = fs.readFileSync(file_path, 'utf8');
  let lines = data.split(/\r?\n/g);
  lines = lines.filter(line => line !== '').map(line => line.replace(/^#+/g, ''));
  let title = lines[0] || "BLANK File";
  let brief = lines.slice(1).join(' ').slice(0, 128);
  return {title: title, brief: brief};
}

module.exports = Item;
