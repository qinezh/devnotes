'use strict';

let events = require('events');
let fs = require('fs');
let path = require('path');
let jade = require('jade');
let util = require('util');

let marked = require('marked');

marked.setOptions({
  highlight: function (code) {
    return require('highlight.js').highlightAuto(code).value;
  }
});

function Detail(jquery_element) {
  events.EventEmitter.call(this);
  this.element = jquery_element;
  this.file_path = "";

  let self = this;
}

util.inherits(Detail, events.EventEmitter);

Detail.prototype.open = function(file_path) {
  if (!file_path) {
    return;
  }

  let self = this;
  fs.readFile(file_path, function(err, data) {
    self.file_path = file_path;
    self.element.empty().append($('<div>').attr('class', 'email-content-body').html(marked(data.toString())));
  });
}

module.exports = Detail;
