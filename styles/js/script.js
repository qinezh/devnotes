'use strict';
global.$ = $;

let chokidar = require('chokidar');

let shell = require('shell');
let fs = require('fs');
let path = require('path');
let exec = require('child_process').exec;
let EventEmitter = require('events').EventEmitter;
let openFolderEvent = new EventEmitter();

const remote = require('electron').remote;
const Menu = remote.Menu;
const MenuItem = remote.MenuItem;
const dialog = remote.dialog;

let Folder = require('./styles/js/folder_view.js');
let Item = require('./styles/js/item_view.js');
let Detail = require('./styles/js/detail_view.js');
let Sortable = require('./styles/js/Sortable.min.js');

let HOME;

var template = [
  {
    label: 'Open',
    submenu: [
      {
        label: "Open Folder",
        click: function(item, focusedWindow) {
          dialog.showOpenDialog({properties: ['openDirectory']}, function(folderPaths) {
            HOME = folderPaths.pop();
            openFolderEvent.emit('openFolderSuccess');
          });
        }
      }
    ]
  },
  {
    label: 'View',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click: function(item, focusedWindow) {
          if (focusedWindow)
            focusedWindow.reload();
        }
      },
      {
        label: 'Toggle Full Screen',
        accelerator: (function() {
          if (process.platform == 'darwin')
            return 'Ctrl+Command+F';
          else
            return 'F11';
        })(),
        click: function(item, focusedWindow) {
          if (focusedWindow)
            focusedWindow.setFullScreen(!focusedWindow.isFullScreen());
        }
      },
    ]
  },
  {
    label: 'Window',
    role: 'window',
    submenu: [
      {
        label: 'Minimize',
        accelerator: 'CmdOrCtrl+M',
        role: 'minimize'
      },
      {
        label: 'Close',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      },
    ]
  },
  {
    label: 'Help',
    role: 'help',
    submenu: [
      {
        label: 'Learn More',
        click: function() { require('electron').shell.openExternal('http://electron.atom.io') }
      },
    ]
  },
];

var menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);

function execPromiseFn(cmd, options) {
  return function() {
    return new Promise(function(resolve, reject) {
      exec(cmd, options, function(err, stdout, stderr) {
        if (err) reject(new Error(err));
        console.log(stdout);
        console.log(stderr);
        resolve();
      });
    });
  }
}

$(function() {
  openFolderEvent.on('openFolderSuccess', function() {
    let folders = new Folder($('#folders'));
    let items = new Item($('#list'));
    let detail = new Detail($("#detail"));

    $('.primary-button.pure-button').click(function() {
      [
        execPromiseFn('git add .', {cwd: '../devnotes/'}),
        execPromiseFn('git commit -m "update"', {cwd: '../devnotes/'}),
        execPromiseFn('git push', {cwd: '../devnotes/'})
      ].reduce((p, fn) => p.then(fn), Promise.resolve())
      .then(function() {
        alert('update successfully!')
      })
      .catch(function(e) {
        console.error(e);
        alert('update failed');
      });
    });

    // initial
    folders.open(HOME, function(default_folder) {
      items.open(default_folder, function(default_file) {
        detail.open(default_file);
      });
    });

    folders.on("list", function(folder) {
      items.open(folder);
    });

    folders.on("open_folder", function(folder) {
      folder = path.join(folder || HOME, '...');
      shell.showItemInFolder(path.resolve(folder));
    });

    items.on("detail", function(file_path) {
      detail.open(file_path);
    });

    items.on("edit", function(file_path) {
      shell.openItem(path.resolve(file_path));
    });

    items.on("open_folder", function(folder) {
      console.log(folder)
      shell.showItemInFolder(path.resolve(folder));
    });

    // event: add, unlink, change, addDir, unlinkDir
    chokidar.watch(HOME, {ignored: /[\/\\]\./}).on('all', (e, p) => {
      switch (e) {
        case "add":
          if (folders.folder_path !== "" && folders.folder_path === path.dirname(p)) {
            items.open(folders.folder_path);
          }
          break;
        case "unlink":
          if (folders.folder_path !== "" && folders.folder_path === path.dirname(p)) {
            items.open(folders.folder_path);
          }
          break;
        case "change":
          if (detail.file_path !== "" && detail.file_path === p) {
            setTimeout(function() {
              items.open(folders.folder_path);
              detail.open(p);
            }, 100);
          } else if (folders.folder_path !== "" && folders.folder_path === path.dirname(p)) {
            items.open(folders.folder_path);
          }
          break;
        case "addDir":
          folders.open(HOME);
          break;
        case "unlinkDir":
          folders.open(HOME);
          break;
      }
    });
    Sortable.create(document.getElementById('folders'));
  });
});
