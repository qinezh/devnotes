'use strict';

let gulp = require('gulp');
let git = require('gulp-git');
let exec = require('child_process').exec;

function execPromiseFn(cmd, options) {
  return function() {
    return new Promise(function(resolve, reject) {
      exec(cmd, options, function(err, stdout, stderr) {
        if (err) reject(new Error(err));
        console.log(stdout);
        console.error(stderr);
        resolve();
      });
    });
  }
}

gulp.task('update-notes', function() {
  [
    execPromiseFn('git add .', {cwd: '../devnotes/'}),
    execPromiseFn('git commit -m "update"', {cwd: '../devnotes/'}),
    execPromiseFn('git push', {cwd: '../devnotes/'})
  ].reduce((p, fn) => p.then(fn), Promise.resolve());
});

gulp.task('update', function() {
  [
    execPromiseFn('git add .'),
    execPromiseFn('git commit -m "update"'),
    execPromiseFn('git push')
  ].reduce((p, fn) => p.then(fn), Promise.resolve());

});

gulp.task('dev', function() {
  exec('npm start');
});

gulp.task('default', ['update-notes']);
